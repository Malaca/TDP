from scipy.optimize import curve_fit as fit
import matplotlib.pyplot as plt
import numpy as np
import math



results=[]


a=[str(i) for i in range(10)]
with open('interferometria_80.txt') as inputfile:
    for line in inputfile:
        if line[0] in a:
            results.append(line.strip().split())

results=np.array(results)

#print(results[0][0])


position=[float(results[i][0]) for i in range(len(results))]
S=[float(results[i][1]) for i in range(len(results))]

Savg=[]
c=0
#for i in range(0, len(position)-1):
i=0
while i<len(position):
	if position[i+1]==position[i]:
		Savg.append((S[i+1]+S[i])/2)
		i=i+1
		c=c+1
	else:
		Savg.append(S[i])
		c=c+1

	i=i+1



scale=32/1601
pos=[float(i) for i in range(c)]
halfindex=int(len(pos)/2-1)
pos1=pos[:halfindex]
pos2=pos[halfindex:]
Savg1=Savg[:halfindex]
Savg2=Savg[halfindex:]
pos1=np.array(pos1)
pos1=scale*pos1
pos2=np.array(pos2)
pos2=scale*pos2


#pi=float(math.pi)

def func(z, A, l, phi, offset):
	return A * np.cos(z*2*math.pi/l+phi)+offset
#parameters, covariance = curve_fit(func, V_data, I_data, p0=(-1.62236837e-1,2.74582821e-02,3.97658018e-2))


#getting range for fitting to the left
p=0
#1.8
for i in range(len(pos1)):
	if pos1[i]>=1.2:
		p=i
		break

pos1=pos1[p:]
Savg1=Savg1[p:]

#10.25
for i in range(len(pos1)):
	if pos1[i]>=9.96:
		p=i
		break

pos1=pos1[:p]
Savg1=Savg1[:p]


#getting range for fitting to the right
#24.019 
for i in range(len(pos2)):
	if pos2[i]>=17.57:
		p=i
		break

pos2=pos2[p:]
Savg2=Savg2[p:]

#29.2
for i in range(len(pos2)):
	if pos2[i]>=22.68:
		p=i
		break

pos2=pos2[:p]
Savg2=Savg2[:p]
pos2=pos2


#fourier1=np.fft.fft(Savg1)
#size1=len(fourier1)
#fourierfreq1=np.fft.fftfreq(size1, scale)




parameters1, covariance1 = fit(func, pos1, Savg1, p0=(300, 10.6, 0, 1582 ))
errors1 = np.sqrt(np.diag(abs(covariance1)))


#parameters2, covariance2 = fit(func, pos2, Savg2, p0=(500, 7.5, 0, 1700 ))
#errors2=np.sqrt(np.diag(abs(covariance2))) 

#parameters2, covariance2 = fit(func, pos2, Savg2, p0=(2, 2, 0, 1779 ))
#errors2=np.sqrt(np.diag(abs(covariance2))) 

#f=open('fitting data', 'w')

#for i in range(len(parameters1)):
#	f.write('



#plt.plot(fourierfreq1, fourier1)
#plt.show()


#print(parameters[0])
#print(parameters[1])
#print(parameters[2])
#print(parameters[3])

print("lambda")
print(parameters1[1])
print("erro de lambda")
print(errors1[1])

pos22=pos[halfindex:]
pos22=np.array(pos22)
pos22=scale*pos22
pos22=pos22-16
S22=Savg[halfindex:]

pos11=pos[:halfindex]
pos11=np.array(pos11)
pos11=scale*pos11
S11=Savg[:halfindex]

pos=np.array(pos)
pos=pos*scale


plt.xlabel('position(cm)')
plt.ylabel('S(z)')
plt.plot(pos, Savg, 'm')
plt.plot(pos1, Savg1, 'r')
plt.plot(pos1, func(pos1, parameters1[0], parameters1[1], parameters1[2], parameters1[3]), 'b')
plt.show()

#A=parameters[0]
#l=parameters[1]
#phi=parameters[2]

#plt.plot(position, func(position, l, phi))
#plt.show()

