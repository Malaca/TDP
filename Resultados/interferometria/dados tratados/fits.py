from scipy.optimize import curve_fit as fit
import matplotlib.pyplot as plt
import numpy as np
import math

results=[]

a=[str(i) for i in range(10)]
with open('k(f)_esquerda3') as inputfile:
    for line in inputfile:
        if line[0] in a:
            results.append(line.strip().split())

results=np.array(results)

results2=[]
with open('k(f)_direita3') as inputfile:
    for line in inputfile:
        if line[0] in a:
            results2.append(line.strip().split())

results2=np.array(results2)
#print(results[0][0])


freq=[float(results[i][0]) for i in range(len(results))]
freq=np.array(freq)
omega=2*math.pi*freq
k=[float(results[i][1]) for i in range(len(results))]
k=np.array(k)
kSI=100*k     #k is in cm^-1
errk=[float(results[i][2]) for i in range(len(results))]
errk=np.array(errk)
errkSI=100*errk

freq2=[float(results2[i][0]) for i in range(len(results2))]
freq2=np.array(freq2)
omega2=2*math.pi*freq2
k2=[float(results2[i][1]) for i in range(len(results2))]
k2=np.array(k2)
kSI2=100*k2
errk2=[float(results2[i][2]) for i in range(len(results2))]
errk2=np.array(errk2)
errkSI2=100*errk2

wce=2*math.pi*300
def func(f, p, wpe):
	return np.array(p/100*np.sqrt((wpe**2/((2*math.pi*f)**2-wce**2)-1)/(1-(wpe/(2*math.pi*f))**2)))

parameters2, covariance2=fit(func, freq2, k2, p0=(240.5, 2*math.pi*150.5), sigma=errk2, absolute_sigma=True) 
errors2=np.sqrt(np.diag(abs(covariance2))) 
residuals2=np.subtract(k2, func(freq2, *parameters2))
residualssq2=np.multiply(residuals2, residuals2)
errk2sq=np.multiply(errk2, errk2)
chisq2=np.sum(np.divide(residualssq2, errk2sq))
ndf2=len(k2)-len(parameters2)
print("para o ajuste á direita o chisq/ndf é:")
print(chisq2/ndf2)
print('E fpe:')
print(parameters2[1]/(2*math.pi))
print('errorfpe:')
print(errors2[1]/(2*math.pi))
print('para o raio da coluna de plasma:')
print(2.405/parameters2[0])
print('erro do raio:')
print(2.405/(parameters2**2)*errors2[0])

parameters1, covariance1=fit(func, freq, k, p0=(240.5, 2*math.pi*200), sigma=errk, absolute_sigma=True) 
errors1=np.sqrt(np.diag(abs(covariance1))) 
residuals1=np.subtract(k, func(freq, *parameters1))
residualssq1=np.multiply(residuals1, residuals1)
errksq=np.multiply(errk, errk)
chisq1=np.sum(np.divide(residualssq1, errksq))
ndf1=len(k)-len(parameters1)
print("para o ajuste á esquerda o chisq/ndf é:")
print(chisq1/ndf1)
print('E fpe:')
print(parameters1[1]/(2*math.pi))
print('errorfpe:')
print(errors1[1]/(2*math.pi))
print('para o raio da coluna de plasma:')
print(2.405/parameters1[0])
print('erro do raio:')
print(2.405/(parameters1[0]**2)*errors1[0])

wpe1=float(parameters1[1])
p1=float(parameters1[0])
def vph(f):
	return 2*math.pi*f/(func(f, p1, wpe1)*100) 


pi=math.pi
def vg(f):
	return 100*(((2*pi*f)**2-wce**2)**2*((2*pi*f)**2-wpe1**2)**2*func(f, p1, wpe1))/(p1**2*2*pi*f*(wce*wpe1)**2*(wce**2+wpe1**2-2*(2*pi*f)**2))


errvph=np.multiply(np.divide(omega, 100*100*np.multiply(k, k)), 100*errk)

errvph2=np.multiply(np.divide(omega2, np.multiply(kSI2, kSI2)), errkSI2)

wpe=2*pi*170
def p(omega, k):
	return np.sqrt(k**2*(-1+wpe**2/(omega**2))/(1-wpe**2/(omega**2-wce**2)))


errp=np.multiply(np.divide(p(omega, k), k), errk)
errp2=np.multiply(np.divide(p(omega2, k2), k2), errk2)
print(vg(150))
print(pi)


###left side
vgarr=[]
errvg=[]
freqvg=[]
for i in range(0,len(freq)-1):
	vgarr.append((omega[i+1]-omega[i])/(kSI[i+1]-kSI[i]))
	errvg.append(abs((omega[i+1]-omega[i])/(kSI[i+1]**2))*errkSI[i+1]+abs((omega[i+1]-omega[i])/(kSI[i]**2)*errkSI[i]))
	freqvg.append(freq[i]+(freq[i+1]-freq[i])/2)

vgarr=np.array(vgarr)
errvg=np.array(errvg)
freqvg=np.array(freqvg)


###right side
vgarr2=[]
errvg2=[]
freqvg2=[]
for i in range(0,len(freq2)-1):
	vgarr2.append((omega2[i+1]-omega2[i])/(kSI2[i+1]-kSI2[i]))
	errvg2.append(abs((omega2[i+1]-omega2[i])/(kSI2[i+1]**2))*errkSI2[i+1]+abs((omega2[i+1]-omega2[i])/(kSI2[i]**2)*errkSI2[i]))
	freqvg2.append(freq2[i]+(freq2[i+1]-freq2[i])/2)

vgarr2=np.array(vgarr2)
errvg2=np.array(errvg2)
freqvg2=np.array(freqvg2)


###right side velocities###
plt.ylabel('v (10^6m/s)')
plt.xlabel('f (MHz)')
plt.plot(freq2, vph(freq2) , 'b')
plt.scatter(freq2, omega2/(k2*100), c='b')
plt.errorbar(freq2, omega2/(k2*100), yerr=errvph2, ecolor='b', linestyle="None")
plt.plot(freq2, vg(freq2), 'r')
plt.errorbar(freqvg2, vgarr2, yerr=errvg2, ecolor='r', linestyle="None")
plt.scatter(freqvg2, vgarr2, c='r')
plt.show()

###left side velocities###

#plt.ylabel('v (10^6m/s)')
#plt.xlabel('f (MHz)')
#plt.plot(freq, vph(freq) , 'b')
#plt.scatter(freq, 2*pi*freq/(k*100), c='b')
#plt.errorbar(freq, 2*pi*freq/(k*100), yerr=errvph, ecolor='b', linestyle="None")
#plt.plot(freqvg, vg(freqvg), 'r')
#plt.errorbar(freqvg, vgarr, yerr=errvg, ecolor='r', linestyle="None")
#plt.scatter(freqvg, vgarr, c='r')
#plt.show()

####pvalues###
plt.ylabel('p(cm^(-1))')
plt.xlabel('f (MHz)')
plt.scatter(freq, p(omega, k), c='r')
plt.errorbar(freq, p(omega, k), yerr=errp, ecolor='r', linestyle="None")
plt.show()

plt.xlabel('f (MHz)')
plt.ylabel('k (1/cm)')
plt.plot(freq2, func(freq2, *parameters2), 'r')
plt.scatter(freq2, k2)
plt.errorbar(freq2,k2,yerr=errk2, ecolor='b', linestyle="None")
plt.scatter(freq2, k2, c='r')
plt.errorbar(freq2, k2, yerr=errk2, ecolor='r', linestyle="None")
plt.show()
