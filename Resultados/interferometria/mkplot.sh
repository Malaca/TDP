#!/bin/sh
for i in dados/*txt
do
	out=plots/$(echo $i|sed 's/.*\///')
	out=${out%txt}png
	gnuplot -e "filename=\"$i\";out=\""$out"\"" plot.gp
done
