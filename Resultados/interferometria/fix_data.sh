#!/bin/sh
for i in dados_orig/*txt
do
	out=dados/$(echo $i|sed 's/.*\///')
	sed -e '1,18d' \
	    -e '/#/d'  \
	    -e 's/2017.*[0-9]\{9\}\t//' \
	    -e 's/,0\{6\}//g' \
	    -e 's/\t/ /' \
	    -e 's/\.//g' \
	    "$i" > $out
done
