from sys import argv
import pickle as pk
import numpy as np
import scipy.signal as sig
import matplotlib
from scipy.fftpack import fft, rfft
from scipy.optimize import curve_fit as fit
matplotlib.use('AGG')
import matplotlib.pyplot as plt
from sdas.tests.LoadSdasData import LoadSdasData
from sdas.tests.StartSdas import StartSdas

def pltsav(filename,*args):
    plt.clf()
    plt.plot(*args)
    plt.savefig(filename)

#execfile("test.py")
#exemplo filtro hp
ts=tvf[1]-tvf[0]
vf_ny = 1e6/(2*ts)
ripple_db=60.0
width=2e3/vf_ny
N, beta = sig.kaiserord(ripple_db, width)
N=2*int(N/2)+2
print "N=",N
b = sig.firwin(N,1e3/vf_ny,window=('kaiser',beta))
b=-b
b[N/2]=1+b[N/2]
w,h=sig.freqz(b)
pltsav("hp.png",(w/np.pi)*vf_ny,np.absolute(h))
a=[0 for _ in range(N)]
a[0]=1
