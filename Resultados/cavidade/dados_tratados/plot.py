from matplotlib import pyplot as plt
import numpy as np
results=[]


a=[str(i) for i in range(10)]
with open('cavidade_vacuo.txt') as inputfile:
    for line in inputfile:
        if line[0] in a:
            results.append(line.strip().split('\t'))

#CH0 -- frequency (results[-][1])
#CH1 -- transmission (results[-][2])
#CH2 -- reflected (results[-][3])
#CH3 -- emitted (results[-][4])
results=np.array(results)

def func(word):
    if word[1]=='.' and word[5]=='.':
        return word[0:1]+word[2:]
    else:
        return word

frequency=[float(func(results[i][1])) for i in range(len(results))]
emission=[float(func(results[i][2])) for i in range(len(results))]
reflection=[float(func(results[i][3])) for i in range(len(results))]
transmission=[float(func(results[i][4])) for i in range(len(results))]


plt.plot(frequency,transmission,label='Transmission')
plt.plot(frequency,reflection,label='Reflection')
plt.plot(frequency, emission,label='Emission')
plt.legend()
plt.grid(True)
plt.show()
